#!/bin/bash
sudo su
apt-get update
apt-get install docker.io -y
systemctl enable docker.service
echo {"exec-opts": ["native.cgroupdriver=systemd"]} > /etc/docker/daemon.json
systemctl restart docker
apt-get install -y apt-transport-https ca-certificates curl
curl -fsSLo /usr/share/keyrings/kubernetes-archive-keyring.gpg https://packages.cloud.google.com/apt/doc/apt-key.gpg
echo "deb [signed-by=/usr/share/keyrings/kubernetes-archive-keyring.gpg] https://apt.kubernetes.io/ kubernetes-xenial main" | sudo tee /etc/apt/sources.list.d/kubernetes.list
apt-get update
apt-get install -y kubelet kubeadm kubectl
apt-mark hold kubelet kubeadm kubectl
kubeadm init --pod-network-cidr=192.168.0.0/16
exit
mkdir -p /home/ubuntu/.kube
sudo su
cp -i /etc/kubernetes/admin.conf /home/ubuntu/.kube/config
exit
chown $(id -u):$(id -g) /home/ubuntu/.kube/config

kubectl create -f https://docs.projectcalico.org/manifests/tigera-operator.yaml
kubectl create -f https://docs.projectcalico.org/manifests/custom-resources.yaml

kubectl get pods -n calico-system
## Wait until each pod has the STATUS of Running.
kubectl taint nodes --all node-role.kubernetes.io/master-

kubectl get nodes -o wide
